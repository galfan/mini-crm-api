var models  = require('../models');
var express = require('express');
var router  = express.Router();

router.get('/', function(req, res) {
  models.DepartmentUser.findAll({
    // include: [ models.User, models.Department ]
  }).then(function(response) {
    res.json({
      data: response
    });
});
});

router.post('/', function(req, res) {
  console.log(req.body)
  models.DepartmentUser.create({
    user_id: req.body.user_id,
    department_id: req.body.department_id
  }).then(function(response) {
    res.json({data: response});
  });
});

router.delete('/:_id', function(req, res) {
  models.DepartmentUser.destroy({
    where: {
      id: req.params._id
    }
  }).then(function(response) {
    res.json({data: response});
  });
});


module.exports = router;