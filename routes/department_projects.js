var models  = require('../models');
var express = require('express');
var router  = express.Router();

router.get('/', function(req, res) {
  models.DepartmentProject.findAll({
    include: [ models.Project, models.Department ]
  }).then(function(response) {
    res.json({
      data: response
    });
});
});

router.post('/', function(req, res) {
  console.log(req.body)
  models.DepartmentProject.create({
    ProjectId: req.body.ProjectId,
    DepartmentId: req.body.DepartmentId
  }).then(function(response) {
    res.json({data: response});
  });
});

router.put('/:_id', function(req, res) {
  console.log(req.body)
  models.DepartmentProject.update({
    ProjectId: req.body.ProjectId,
    DepartmentId: req.body.DepartmentId
  }, {
    where: {
      id: req.params._id
    }
  })
  .then(() => models.DepartmentProject.findByPk(req.params._id))
  .then(function(response) {
    res.json({data: response});
  });
});

router.delete('/:_id', function(req, res) {
  models.DepartmentProject.destroy({
    where: {
      id: req.params._id
    }
  }).then(function(response) {
    res.json({data: response});
  });
});


module.exports = router;