var models  = require('../models');
var express = require('express');
var router  = express.Router();

router.get('/', function(req, res) {
  models.Department.findAll({
    // include: [ models.DepartmentProject ]
  }).then(function(response) {
    res.json({
      data: response
    });
});
});

router.post('/', function(req, res) {
  console.log(req.body)
  models.Department.create({
    name: req.body.name,
    description: req.body.description
  }).then(function(response) {
    res.json({data: response});
  });
});
router.put('/:_id', function(req, res) {
  console.log(req.body)
  models.Department.update({
    name: req.body.name,
    description: req.body.description
  }, {
    where: {
      id: req.params._id
    }
  }).then(() => models.Department.findByPk(req.params._id))
  .then(function(response) {
    res.json({data: response});
  });
});

router.delete('/:_id', function(req, res) {
  models.Department.destroy({
    where: {
      id: req.params._id
    }
  }).then(function(response) {
    res.json({data: response});
  });
});


module.exports = router;