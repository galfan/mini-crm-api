var models  = require('../models');
var express = require('express');
var router  = express.Router();

router.get('/', function(req, res) {
  models.TaskType.findAll()
    .then(function(response) {
    res.json({
      data: response
    });
});
});

router.post('/', function(req, res) {
  console.log(req.body)
  models.TaskType.create({
    name: req.body.name,
    description: req.body.description,
  }).then(function(response) {
    res.json({data: response});
  });
});

router.delete('/:_id', function(req, res) {
  models.TaskType.destroy({
    where: {
      id: req.params._id
    }
  }).then(function(response) {
    res.json({data: response});
  });
});


module.exports = router;