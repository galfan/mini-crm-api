var models  = require('../models');
var express = require('express');
var router  = express.Router();

router.get('/', function(req, res) {
  models.User.findAll()
    .then(function(users) {
      res.json({
        data: users
      });
    });
});

router.post('/', function(req, res) {
  console.log(req.body)
  models.User.create({
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    nick_name: req.body.nick_name,
    role_id: req.body.role_id,
  }).then(function(response) {
    res.json({data: response});
  });
});

router.put('/:user_id', function(req, res) {
  models.User.update({
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    nick_name: req.body.nick_name,
    role_id: req.body.role_id,
  }, {where: {
    id: req.params.user_id
  }})
  .then(() => models.User.findByPk(req.params.user_id))
  .then(function(response) {
    res.json({data: response});
  });
});

router.delete('/:user_id', function(req, res) {
  models.User.destroy({
    where: {
      id: req.params.user_id
    }
  }).then(function(response) {
    res.json({data: response});
  });
});


module.exports = router;