var models  = require('../models');
var express = require('express');
var router  = express.Router();

router.get('/', function(req, res) {
  models.Role.findAll()
    .then(function(response) {
    res.json({
      data: response
    });
});
});

router.post('/', function(req, res) {
  console.log(req.body)
  models.Role.create({
    name: req.body.name,
  }).then(function(response) {
    res.json({data: response});
  });
});

router.delete('/:_id', function(req, res) {
  models.Role.destroy({
    where: {
      id: req.params._id
    }
  }).then(function(response) {
    res.json({data: response});
  });
});


module.exports = router;