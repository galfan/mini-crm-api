var models  = require('../models');
var express = require('express');
var router  = express.Router();

router.get('/', function(req, res) {
  models.Project.findAll({
    include: [ models.DepartmentProject ]
  })
    .then(function(projects) {
    res.json({
      data: projects
    });
});
});

router.post('/', function(req, res) {
  models.Project.create({
    name: req.body.name,
    subname: req.body.subname,
    description: req.body.description,
    status: req.body.status,
  }).then(function(response) {
    res.json({data: response});
  });
});

router.put('/:project_id', function(req, res) {
  models.Project.update({
    name: req.body.name,
    subname: req.body.subname,
    description: req.body.description,
    status: req.body.status,
  }, {where: {
    id: req.params.project_id
  }})
  .then(() => models.Project.findByPk(req.params.project_id))
  .then(function(response) {
    res.json({data: response});
  });
});


router.delete('/:project_id', function(req, res) {
  models.Project.destroy({
    where: {
      id: req.params.project_id
    }
  }).then(function(response) {
    res.json({data: response});
  });
});


module.exports = router;