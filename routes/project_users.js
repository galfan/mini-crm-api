var models  = require('../models');
var express = require('express');
var router  = express.Router();

router.get('/', function(req, res) {
  models.ProjectUser.findAll({
    // include: [ models.User, models.Project ]
  }).then(function(response) {
    res.json({
      data: response
    });
});
});

router.post('/', function(req, res) {
  console.log(req.body)
  models.ProjectUser.create({
    project_id: req.body.project_id,
    user_id: req.body.user_id
  }).then(function(response) {
    res.json({data: response});
  });
});

router.delete('/:_id', function(req, res) {
  models.ProjectUser.destroy({
    where: {
      id: req.params._id
    }
  }).then(function(response) {
    res.json({data: response});
  });
});


module.exports = router;