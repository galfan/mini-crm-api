var express = require('express');
var router  = express.Router();
var models  = require('../models');

router.post('/login', (req, res, next) => {
  models.User.findOne({where : {
    email: req.body.email,
    password: req.body.password
  }}).then(response => {
    res.json({data: response})
  })
})

module.exports = router;