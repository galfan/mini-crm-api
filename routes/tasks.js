var models  = require('../models');
var express = require('express');
var router  = express.Router();

router.get('/', function(req, res) {
  models.Task.findAll(
    // {include: [ models.Project, models.User, models.TaskType ]}
  ).then(function(response) {
    res.json({
      data: response
    });
});
});

router.post('/', function(req, res) {
  console.log(req.body)
  models.Task.create({
    name: req.body.name,
    description: req.body.description,
    date: req.body.date,
    delivery_date: req.body.delivery_date,
  }).then(function(response) {
    res.json({data: response});
  });
});
router.put('/:_id', function(req, res) {
  console.log(req.body)
  models.Task.update({
    name: req.body.name,
    description: req.body.description,
    date: req.body.date,
    delivery_date: req.body.delivery_date,
  }, {
    where: {
      id: req.params._id
    }
  })
  .then(() => models.Task.findByPk(req.params._id))
  .then(function(response) {
    res.json({data: response});
  });
});

router.delete('/:_id', function(req, res) {
  models.Task.destroy({
    where: {
      id: req.params._id
    }
  }).then(function(response) {
    res.json({data: response});
  });
});


module.exports = router;