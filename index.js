const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');

const port = process.env.PORT || 8080;

const server = express()

server.use(bodyParser.json()); 
server.use(bodyParser.urlencoded({ extended: true }));
server.use(cors());

server.get('/', function(req, res) {
    res.send('Welcome to my first API REST deployed with Google cloud Platform! \n add `/api/users` and watch the magic');
});
const users  = require('./routes/users');
const department_projects  = require('./routes/department_projects');
const department_users  = require('./routes/department_users');
const departments  = require('./routes/departments');
const project_users  = require('./routes/project_users');
const projects  = require('./routes/projects');
const roles  = require('./routes/roles');
const task_types  = require('./routes/task_types');
const tasks  = require('./routes/tasks');
const auth  = require('./routes/auth');


server.use('/api/users', users);
server.use('/api/department_projects', department_projects);
server.use('/api/department_users', department_users);
server.use('/api/departments', departments);
server.use('/api/project_users', project_users);
server.use('/api/projects', projects);
server.use('/api/roles', roles);
server.use('/api/task_types', task_types);
server.use('/api/tasks', tasks);
server.use('/api/auth', auth);

server.listen(port, () => console.log('API SERVER ITS RUNNING ON PORT -> ', port))