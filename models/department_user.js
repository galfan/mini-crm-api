'use strict';
module.exports = (sequelize, DataTypes) => {
  var DepartmentUser = sequelize.define('DepartmentUser', {
  });

  DepartmentUser.associate = function (models) {
    models.DepartmentUser.belongsTo(models.User);
    models.DepartmentUser.belongsTo(models.Department);
  };

  return DepartmentUser;
};