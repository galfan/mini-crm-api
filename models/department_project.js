'use strict';
module.exports = (sequelize, DataTypes) => {
  var DepartmentProject = sequelize.define('DepartmentProject', {
  });

  DepartmentProject.associate = function (models) {
    models.DepartmentProject.belongsTo(models.Project);
    models.DepartmentProject.belongsTo(models.Department);
  };

  return DepartmentProject;
};