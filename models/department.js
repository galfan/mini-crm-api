'use strict';
module.exports = (sequelize, DataTypes) => {
  var Department = sequelize.define('Department', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
  });
  return Department;
};