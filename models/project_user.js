'use strict';
module.exports = (sequelize, DataTypes) => {
  var ProjectUser = sequelize.define('ProjectUser', {
  });

  ProjectUser.associate = function (models) {
    models.ProjectUser.belongsTo(models.User);
    models.ProjectUser.belongsTo(models.Project);
  };

  return ProjectUser;
};