'use strict';
module.exports = (sequelize, DataTypes) => {
  var Task = sequelize.define('Task', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    date: DataTypes.DATE,
    delivery_date: DataTypes.DATE,
  });
  Task.associate = function (models) {
    models.Task.belongsTo(models.User);
    models.Task.belongsTo(models.Project);
    models.Task.belongsTo(models.TaskType);
  }
  return Task;
};