'use strict';
module.exports = (sequelize, DataTypes) => {
  var TaskType = sequelize.define('TaskType', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
  });

  return TaskType;
};