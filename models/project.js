'use strict';
module.exports = (sequelize, DataTypes) => {
  var Project = sequelize.define('Project', {
    name: DataTypes.STRING,
    subname: DataTypes.STRING,
    description: DataTypes.STRING,
    status: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    }
  });
  Project.associate = function (models) {
    models.Project.hasMany(models.DepartmentProject);
  };
  return Project;
};