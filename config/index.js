require('dotenv').config();

module.exports = {
    development: {
      dialect: process.env.DB_DIALECT,
      database: process.env.DATABASE_NAME,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      host: process.env.DB_HOST || '127.0.0.1',
    },
    production: {
      dialect: process.env.DB_DIALECT,
      database: process.env.DATABASE_NAME,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      host: process.env.DB_HOST,
      dialectOptions: {
        socketPath: `/cloudsql/${process.env.INSTANCE_CONNECTION_NAME}`
      }
    }
}